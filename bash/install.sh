rm ~/.bashrc
ln -s $(pwd)/.bashrc ~/.bashrc

# Install bash git prompt
BASHGITPROMPT_DIRECTORY=~/.bash-git-prompt
if [ ! -d $BASHGITPROMPT_DIRECTORY ]; then

    echo "Installing bash-git-prompt..."
    cd ~
    git clone https://github.com/magicmonty/bash-git-prompt.git .bash-git-prompt --depth=1
    bash
else 
    echo "Folder ~/.bash-git-prompt already exists. Skipping..."
fi

# Create code folder symlink if running Windows Subsystem for Linux
if [ 'grep -q "Microsoft" /proc/version' ]; then
    ln -s /mnt/c/code ~/code
fi

# create symlink for custom theme
ln -s $(pwd)/MikeCustom.bgptheme ~/.bash-git-prompt/themes/MikeCustom.bgptheme

# install commonly used packages
sudo apt-get --assume-yes install unzip

# add custom vimrc
ln -s $(pwd)/.vimrc ~/.vimrc

# install node (via nvm)
touch ~/.bashrc
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
// restart bash
nvm install node
